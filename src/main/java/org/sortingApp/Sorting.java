package org.sortingApp;

/**
 * Sorting class
 */

public class Sorting {

    public static void sort(String[] args) {

        if (args.length > 10 || args.length == 0) {
            throw new IllegalArgumentException();
        }

        int[] array = new int[args.length];

        try {
            for (int i = 0; i < args.length; i++) {
                array[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException();
        }

        System.out.print(sortArray(array).trim());
    }

    public static String sortArray(int[] array) {

        int pivot;
        int temp;

        for (int i = 0; i < array.length; i++) {

            pivot = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[pivot] > array[j]) {
                    pivot = j;
                }
            }

            temp = array[pivot];
            array[pivot] = array[i];
            array[i] = temp;
        }

        StringBuilder output = new StringBuilder();

        for (int j : array) {
            output.append(" ").append(j);
        }

        return output.toString();
    }
}
