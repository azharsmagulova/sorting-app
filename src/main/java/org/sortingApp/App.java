package org.sortingApp;

/**
 * Main class
 */
public class App 
{
    public static void main(String[] args ) {

        Sorting.sort(args);
    }
}
