package org.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


/**
 * This class is supposed to check for illegal arguments.
 * First two tests are supposed to fail in order to make sure that legal arguments actually provide correct outputs.
 * The rest of the tests should pass.
 */

@RunWith(Parameterized.class)
public class IllegalInputCaseTest {

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][] {
                {"0, 18, 9"},
                {"0, 9, 18, 1000, 200, 100, 10, 2350, 45000, -11"},
                {""},
                {"0, 9, 18, 1000, 200, 100, 10, 2350, 45000, 11, -38, 4"},
                {"=, 34, 13"},
                {"g, 700"},
                {"0, !, 12"},
                {"0.17"},
                {"3000000000"}
        });
    }

    private String input;

    public IllegalInputCaseTest(String input) {
        this.input = input;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroACaseCase() {
        String[] input = this.input.split("\\s*,\\s*");
        Sorting.sort(input);
    }
}
