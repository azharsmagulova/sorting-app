package org.sortingApp;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.PrintStream;
import java.util.stream.Stream;

/**
 * This is the bonus testing file with the different JUnit version (5.6.2)
 * All arguments are legal and all tests should pass
 */

public class RegularInputCaseTestJUnit5 {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void test(String[] argsInput, String output) {
        Sorting.sort(argsInput);
        Assertions.assertEquals(output, outputStreamCaptor.toString().trim());

    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    public static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(new String[]{"0"}, "0"),
                Arguments.of(new String[]{"2",  "1"}, "1 2"),
                Arguments.of(new String[]{"2", "13809",  "1"}, "1 2 13809"),
                Arguments.of(new String[]{"3509", "-11", "1309", "10",}, "-11 10 1309 3509"),
                Arguments.of(new String[]{"5", "-112000",  "7", "5", "-112000"}, "-112000 -112000 5 5 7"),
                Arguments.of(new String[]{"35999", "1089", "-1789", "124", "-18096", "23", "124", "-2", "1089", "28970964"},
                        "-18096 -1789 -2 23 124 124 1089 1089 35999 28970964")
        );
    }
}
