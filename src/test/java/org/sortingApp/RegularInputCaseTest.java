package org.sortingApp;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

/**
 * This is the file for legal argument test cases and all tests should pass.
 */

@RunWith(Parameterized.class)
public class RegularInputCaseTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][] {
                {"9", "9"},
                {"34, 23, 34", "23 34 34"},
                {"-234, 179, -1000, 1792", "-1000 -234 179 1792"},
                {"12, -178, 234, 12, 5, 9087, 289765, -19865976, 12, -178", "-19865976 -178 -178 5 12 12 12 234 9087 289765"}
        });
    }

    private String input;
    private String expected;

    public RegularInputCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void test() {
        String[] input = this.input.split("\\s*,\\s*");
        Sorting.sort(input);

        Assert.assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }
}
